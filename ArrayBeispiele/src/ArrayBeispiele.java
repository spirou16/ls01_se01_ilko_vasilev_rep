
public class ArrayBeispiele {

	public static void main(String[] args) {
		
		int[]	zliste = new int[4];
		
		zliste[0] = 7;
		zliste[1] = 0;
		zliste[2] = 5;
		zliste[3] = 0;
		
		System.out.print(zliste[0] + " ");
		System.out.print(zliste[1] + " ");
		System.out.print(zliste[2] + " ");
		System.out.print(zliste[3] + " ");
		
		for (int i=0 ; i < zliste.length ; i++) {
			zliste[i] = 2 * i + 1;	
		}
		
		for (int i=0 ; i < zliste.length ; i--	) {
			System.out.print(zliste[i] + " ");
			
		}
		
	}

}
