import java.util.Scanner;

public class ArrayBeispiele2 {

	public static void main(String[] args) {
		
		Scanner Tastatur = new Scanner (System.in);
		System.out.println("Wieviel Zahlen m�chten sie eingeben ");
		int anzahl=Tastatur.nextInt();
		
		int[] zliste = new int[anzahl];
		
		for(int i=0; i<anzahl; i++) {
			System.out.println((i +1) + ". Zahl:");
			zliste[i] = Tastatur.nextInt();
		}
		
		System.out.print("[ ");

		for(int i=0; i<anzahl; i++){
			System.out.print(zliste[i]+" ");
			
		}
		System.out.print("]");
		
	}

}