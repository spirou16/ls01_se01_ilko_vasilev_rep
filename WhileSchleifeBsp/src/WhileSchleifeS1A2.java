import java.util.Scanner;
public class WhileSchleifeS1A2 {
	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner myScanner = new Scanner (System.in)) {
			System.out.println("Bis welche Zahl soll ausgegeben werden?");
			int bisZahl = myScanner.nextInt();
			
			while (1 <= bisZahl ) {	
				System.out.print(bisZahl + ",");
				bisZahl--;
				
			}
		}
		
	}
}