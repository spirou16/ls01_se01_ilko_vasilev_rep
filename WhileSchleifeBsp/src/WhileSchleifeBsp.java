import java.util.Scanner;
public class WhileSchleifeBsp {
	
	public static void main(String[] args) {
		try (// TODO Auto-generated method stub
		Scanner myScanner = new Scanner (System.in)) {
			System.out.println("Bis welche Zahl soll ausgegeben werden?");
			
			int bisZahl = myScanner.nextInt();
			int n = 1;
			
			while (n <= bisZahl) {
				
				System.out.println(n);
				n++;

			}
		}
		
	}
}