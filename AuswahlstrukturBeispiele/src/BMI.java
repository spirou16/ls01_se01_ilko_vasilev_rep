import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Geben Sie geschlecht an m/w");
		char geschlecht = scan.next().charAt(0);
		System.out.println("Geben Sie ihre Körpergröße in m an");
		double groesse = scan.nextDouble();
		System.out.println("Geben Sie ihr Gewicht in kg an");
		double gewicht = scan.nextDouble();
		bmi(groesse, gewicht, geschlecht);
		
	}

	public static void bmi(double gr, double ge, char geschlecht) {
		double bmi = ge / (gr * gr);
		if(geschlecht == 'm') {
			if(bmi < 20) {
				System.out.println("Untergewicht");
			}else {
				if(bmi > 25) {
					System.out.println("Übergewicht");
				}else {
					System.out.println("Normalgewicht");
				}
			}
		} else {
			if(bmi < 19) {
				System.out.println("Untergewicht");
			}else {
				if(bmi > 24) {
					System.out.println("Übergewicht");
				}else {
					System.out.println("Normalgewicht");
				}
			}
		}
		
	}
}